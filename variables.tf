variable "prefix" {
  type    = string
  default = "iugu"
}

variable "project" {
  type    = string
  default = "iugu-main-infra"
}

variable "contact" {
  type    = string
  default = "webmaster@iugu.com.br"
}

##### STATIC VARIABLES VALUES
variable "aws_region" {
  type    = string
  default = "us-east-1"
}
variable "aws_s3_bucket" {
  type    = string
  default = "tf-iugu-main-s3-tfstate"
}
variable "aws_s3_bucket_key" {
  type    = string
  default = "tf-iugu-main/tfstate.tfstate"
}
variable "aws_s3_encryption" {
  type    = bool
  default = true
}
variable "aws_dynamodb_lock_table" {
  type    = string
  default = ""
}


variable "main_vpc" {
  type    = string
  default = "iugu-main-vpc"
}
variable "main_internet_gateway" {
  type    = string
  default = "iugu-main-internet-gateway"
}
variable "main_security_group" {
  type    = string
  default = "iugu-main-allowed-security-group"
}
variable "main_db_security_group" {
  type    = string
  default = "iugu-main-db-security-group"
}
variable "main_db_subnet_group" {
  type    = string
  default = "iugu-main-db-subnet-group"
}
variable "main_elastic_ip" {
  type    = string
  default = "iugu-main-elastic-ip"
}
variable "main_subnet_public" {
  type    = string
  default = "iugu-main-vpc-public-subnet"
}
variable "main_subnet_private" {
  type    = string
  default = "iugu-main-vpc-private-subnet"
}
variable "main_route_table" {
  type    = string
  default = "iugu-main-route-table"
}

variable "main_ec2_bastion" {
  type    = string
  default = "iugu-main-ec2-bastion"
}

variable "private_key_name" {
  type    = string
  default = "iugu_key"
}

####### VARIABLES FOR DYNAMIC CONFIGURATION
// ec2_count
variable "ec2" {
  type = map(any)
  default = {
    "bastion" = {
      count = 1,
      name  = "iugu-main-ec2-bastion"
    },
    "app" = {
      count = 1,
      name  = "iugu-main-ec2-app"
    },
    "build" = {
      count = 1,
      name  = "iugu-main-ec2-build"
    },
  }
}

// this var is to set the AMI for the EC2
variable "ec2_ami" {
  type = map(any)
  default = {
    "app" = {
      sample = "ami-0d94f098994f9086f" # ami here!!!!
    },
    "us-east-1" = {
      ubuntu = "ami-053b0d53c279acc90"
      amazon = "ami-05e411cf591b5c9f6"
      redhat = "ami-026ebd4cfe2c043b2"
    },
    "sa-east-1" = {
      ubuntu = "ami-0af6e9042ea5a4e3e"
      amazon = "ami-012f474aac5ae1985"
      redhat = "ami-025379b05972d9189"
    }
  }
}

// T2 instances types
variable "ec2_t2" {
  type = map(any)
  default = {
    micro  = "t2.micro"
    small  = "t2.small"
    medium = "t2.medium"
    large  = "t2.large"
  }
}
#
// T3 instances types
variable "ec2_t3" {
  type = map(any)
  default = {
    micro  = "t3.micro"
    small  = "t3.small"
    medium = "t3.medium"
    large  = "t3.large"
  }
}

// this var is to set the ingress ports for the security group EC2
variable "ingress" {
  type    = list(number)
  default = [80, 443, 22]
}
// this var is to set the egress ports for the security group EC2
variable "egress" {
  type    = list(number)
  default = [0]
}

// this var is to set the CIDR block for the VPC
variable "vpc_cidr_block" {
  description = "CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

// this var is to set the CIDR block for the public subnet
variable "subnet_count" {
  description = "Number of subnets to create"
  type        = map(number)
  default = {
    public  = 1
    private = 2
  }
}

// this var is to set the CIDR block for the public subnet
variable "public_subnet_cidr_blocks" {
  description = "Available CIDR blocks for public subnets"
  type        = list(string)
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",
    "10.0.4.0/24",
  ]
}
// this var is to set the CIDR block for the private subnet
variable "private_subnet_cidr_blocks" {
  description = "Available CIDR blocks for private subnets"
  type        = list(string)
  default = [
    "10.0.101.0/24",
    "10.0.102.0/24",
    "10.0.103.0/24",
    "10.0.104.0/24",
  ]
}

#
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}


