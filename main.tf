provider "aws" {
  region = var.aws_region
}
data "aws_availability_zones" "available" {
  state = "available"
}

# key pair
# no needed for now, we will use api-gateway with LB for a private subnet access


# vpc
resource "aws_vpc" "main_vpc" {

  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags,
    { Name = "${var.main_vpc}" }
  )
}

# igw
resource "aws_internet_gateway" "main_igw" {
  vpc_id = aws_vpc.main_vpc.id
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.main_internet_gateway}" })
  )
}

# subnet
resource "aws_subnet" "main_subnet_public" {
  vpc_id                  = aws_vpc.main_vpc.id
  cidr_block              = var.public_subnet_cidr_blocks[0]
  availability_zone       = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = true
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.main_subnet_public}" })
  )
}
resource "aws_subnet" "main_subnet_private" {
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = var.private_subnet_cidr_blocks[0]
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.main_subnet_private}" })
  )
}

# eip
resource "aws_eip" "iugu_ec2_eip" {
  domain = "vpc"
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.main_elastic_ip}" })
  )
}
# nat gateway
resource "aws_nat_gateway" "main_nat_gateway" {
  allocation_id = aws_eip.iugu_ec2_eip.id
  subnet_id     = aws_subnet.main_subnet_public.id
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-nat-gateway-public" })
  )
  depends_on = [aws_internet_gateway.main_igw]
}


# route table
resource "aws_route_table" "main_route_table_public" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_igw.id
  }
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.main_route_table}-public" })
  )
}
resource "aws_route_table" "main_route_table_private" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main_nat_gateway.id
  }
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${var.main_route_table}-private" })
  )
}

# route association
resource "aws_route_table_association" "route_table_public" {
  route_table_id = aws_route_table.main_route_table_public.id
  subnet_id      = aws_subnet.main_subnet_public.id
}
resource "aws_route_table_association" "route_table_private" {
  route_table_id = aws_route_table.main_route_table_private.id
  subnet_id      = aws_subnet.main_subnet_private.id
}


# run main.tf, then copy applications.tf below and run again

# security group
resource "aws_security_group" "sg_app" {
  name        = "sg_app"
  description = "Allow API Access"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description = "Allow Api Gateway - Health Checks"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.main_vpc.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ec2 instance
resource "aws_instance" "ec2_app" {
  ami                         = var.ec2_ami["app"]["sample"] # or ami-000000000000000 generated 
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.main_subnet_private.id # private subnet
  vpc_security_group_ids      = [aws_security_group.sg_app.id]
  associate_public_ip_address = true
  tags = merge(
    local.common_tags,
    tomap({ Name = "${var.ec2["app"]["name"]}" })
  )
}

# ELB
resource "aws_lb_target_group" "lb_app" {
  name     = "pgto-lb-target-group-app"
  port     = 8080
  protocol = "TCP"
  vpc_id   = aws_vpc.main_vpc.id

  health_check {
    enabled  = true
    protocol = "HTTP"
    path     = "/health"
  }
}
resource "aws_lb_target_group_attachment" "lb_app" {
  target_group_arn = aws_lb_target_group.lb_app.arn
  target_id        = aws_instance.ec2_app.id
  port             = 8080
}
resource "aws_lb" "lb_app" {
  name               = "lb-app"
  internal           = true
  load_balancer_type = "network"
  subnets = [
    aws_subnet.main_subnet_private.id
    //aws_subnet.main_subnet_private2.id,
  ]
}
resource "aws_lb_listener" "lb_app" {
  load_balancer_arn = aws_lb.lb_app.arn
  port              = "8080"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_app.arn
  }
}


# api-gateway
resource "aws_apigatewayv2_api" "api_gw_app" {
  name          = "api-gw-app"
  protocol_type = "HTTP"
  cors_configuration {
    allow_credentials = false
    allow_headers     = []
    allow_methods = [
      "GET",
      "HEAD",
      "OPTIONS",
      "POST",
    ]
    allow_origins = [
      "*",
    ]
    expose_headers = []
    max_age        = 0
  }
}
resource "aws_apigatewayv2_stage" "staging" {
  api_id      = aws_apigatewayv2_api.api_gw_app.id
  name        = "api" # prod, dev, staging
  auto_deploy = true
}
resource "aws_apigatewayv2_vpc_link" "api_gw_app" {
  name               = "vpc-link-app"
  security_group_ids = [aws_security_group.sg_app.id]
  subnet_ids = [
    aws_subnet.main_subnet_private.id
    //aws_subnet.main_subnet_private2.id,
  ]
}
resource "aws_apigatewayv2_integration" "api_gw_app" {
  api_id             = aws_apigatewayv2_api.api_gw_app.id
  integration_uri    = aws_lb_listener.lb_app.arn
  integration_type   = "HTTP_PROXY"
  integration_method = "ANY"
  connection_type    = "VPC_LINK"
  connection_id      = aws_apigatewayv2_vpc_link.api_gw_app.id
}
resource "aws_apigatewayv2_route" "api_gw_app" {
  api_id = aws_apigatewayv2_api.api_gw_app.id

  route_key = "ANY /{proxy+}"
  target    = "integrations/${aws_apigatewayv2_integration.api_gw_app.id}"
}

output "name" {
  value = aws_instance.ec2_app.id
}
